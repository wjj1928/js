function savepng(svg) {
    console.log("savepng")
  svg.on("dblclick",function(d){
    save_bbox_png(svg)
  });
}

function save_bbox_png(svg) {
    console.log("save_bbox_png")
  var doctype = '<?xml version="1.0" standalone="no"?>'
  + '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">';

  svg.style('border','none')

  var margin = 0
  var box = svg.node().getBoundingClientRect()
  box = svg.node().getBBox()
box.x -= margin
box.y -= margin
box.width += 2*margin+2
box.height +=2*margin+2

  svg.attr('viewBox',`${box.x} ${box.y} ${box.width} ${box.height}`)

    var ratio = 2048/box.height
    var height = box.height*ratio
    var width = height*box.width/box.height

  svg.attr('width',width)
  svg.attr('height',height)

  var source = (new XMLSerializer()).serializeToString(svg.node());
  var blob = new Blob([ doctype + source], { type: 'image/svg+xml;charset=utf-8' });
  var url = window.URL.createObjectURL(blob);
var img = d3.select('body').append('img')
  .attr('width', width)
.attr('height', height)
.style('margin',0)
  .node();

  img.onload = function(){
var canvas = d3.select('body').append('canvas')
.style('margin',0)
.node();
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext('2d');
    ctx.drawImage(img, 0, 0);
    var canvasUrl = canvas.toDataURL("image/png");
    downloadme(canvasUrl)
    document.body.removeChild(img);
  }
  img.src = url;
}

function downloadme(svgUrl) {
  var downloadLink = document.createElement("a");
  downloadLink.href = svgUrl;
  downloadLink.download = "save.png"
  document.body.appendChild(downloadLink);
  downloadLink.click();
  document.body.removeChild(downloadLink);
}

